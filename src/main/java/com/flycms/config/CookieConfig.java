package com.flycms.config;

/**
 * Open source house, All rights reserved
 * 开发公司：28844.com<br/>
 * 版权：开源中国<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 14:14 2018/7/8
 */
public class CookieConfig {

	private String domain;
	private int attendanceMaxAge;
	// attendance name
	private String attendanceName;
	private int rememberMeExpireDay;

	public int getRememberMeExpireDay() {
		return rememberMeExpireDay;
	}

	public void setRememberMeExpireDay(int rememberMeExpireDay) {
		this.rememberMeExpireDay = rememberMeExpireDay;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getAttendanceMaxAge() {
		return attendanceMaxAge;
	}

	public void setAttendanceMaxAge(int attendanceMaxAge) {
		this.attendanceMaxAge = attendanceMaxAge;
	}

	public String getAttendanceName() {
		return attendanceName;
	}

	public void setAttendanceName(String attendanceName) {
		this.attendanceName = attendanceName;
	}
}
